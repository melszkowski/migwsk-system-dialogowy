package weather;

import com.github.fedy2.weather.YahooWeatherService;
import com.github.fedy2.weather.data.Channel;
import com.github.fedy2.weather.data.Condition;
import com.github.fedy2.weather.data.Wind;
import com.github.fedy2.weather.data.unit.DegreeUnit;

public class WeatherForecaster {

    public String getWeatherForecast() {

        StringBuilder sb = new StringBuilder();
        Condition con;
        Wind wind;
        try {
            YahooWeatherService service = new YahooWeatherService();
            //Szczecin: woeid == 521598
            Channel channel = service.getForecast("521598", DegreeUnit.CELSIUS);

            con = channel.getItem().getCondition();
            sb.append(con.getText() + ". ");
            sb.append("Temperature is " + con.getTemp() + "Celsius degrees. ");

            wind = channel.getWind();
            //ignore wind if it's weak:
            if (wind.getSpeed() > 1.0) {
                sb.append("The wind is blowing at speed " + wind.getSpeed());

                String windUnit = channel.getUnits().getSpeed().toString();
                if (windUnit == "MPH") {
                    sb.append(" miles per hour.");
                } else {
                    sb.append(" kilometers per hour.");
                }
                sb.append("Going " + getWindDirection(wind.getDirection()) + ". ");
            }

            return sb.toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return "Unable to get weather data.";
        }
    }

    private String getWindDirection(int degrees) {
        if(degrees >= 45 && degrees < 135){
            return "east";
        } else if (degrees >= 135 && degrees < 225){
            return "south";
        } else if (degrees >= 225 && degrees < 315){
            return "west";
        } else {
            return "north";
        }
    }

}
