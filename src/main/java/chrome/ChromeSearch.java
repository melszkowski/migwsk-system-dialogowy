package chrome;
import java.io.IOException;

import launcher.*;
import org.jetbrains.annotations.NotNull;
import rec.Recognition;

public class ChromeSearch {
        public static Recognition rec;
    static Closeable c;
    AppState s;
    Thread chromeThread;
    String search;
    public ChromeSearch() {
    }
    public void accept(@NotNull String input) throws IOException {

            switch (input.toUpperCase()) {
                case "CHROME":
                    Runtime.getRuntime().exec("cmd /c start chrome.exe");
                    break;
                case "GOOGLE":
                    Runtime.getRuntime().exec("cmd /c start chrome.exe google.pl");
                    break;
                case "YOUTUBE":
                    Runtime.getRuntime().exec("cmd /c start chrome.exe youtube.pl");
                    break;
                case "FACEBOOK":
                    Runtime.getRuntime().exec("cmd /c start chrome.exe facebook.com");
                    break;
                case "MUSIC":
                    Runtime.getRuntime().exec("cmd /c start chrome.exe spotify.com");
                    break;
                case "DEAN'S OFFICE":
                    Runtime.getRuntime().exec("cmd /c start chrome.exe e-dziekanat.zut.edu.pl");
                    break;
                case "MAIL":
                    Runtime.getRuntime().exec("cmd /c start chrome.exe mail.google.com");
                    break;
                case "MAPS":
                    Runtime.getRuntime().exec("cmd /c start chrome.exe maps.google.com");
                    break;
                case "TV":
                    Runtime.getRuntime().exec("cmd /c start chrome.exe netflix.com");
                    break;
                default:
                    System.out.println("Wrong option");
                    break;
            }
    }
    public void close() {

    }
}
