package rec;

import java.io.IOException;

@FunctionalInterface
public interface Hearable {
    void hear(String message) throws IOException;
}
