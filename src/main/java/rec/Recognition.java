package rec;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;

import java.io.IOException;
import java.util.Scanner;

public class Recognition implements Runnable {

    public Hearable h;
    public String word;
    public Recognition(Hearable h) {
        this.h = h;
    }
    Configuration configuration = new Configuration();
    public void run() {
        System.out.println("Started recognition thread");
        configuration.setAcousticModelPath("en-us-custom");
        configuration.setDictionaryPath("en-us-custom.dict");
        configuration.setLanguageModelPath("en-us-custom.lm");

        try {
            LiveSpeechRecognizer recognizer = new LiveSpeechRecognizer(configuration);
            recognizer.startRecognition(true);
            SpeechResult result;

            while ((result = recognizer.getResult()) != null) {
                if (result.getHypothesis().length() > 0) {
                    System.out.format("Hypothesis: %s\n", result.getHypothesis());
                    this.setWord(result.getHypothesis());
                    h.hear(result.getHypothesis().toLowerCase());
                }
            }
            recognizer.stopRecognition();
           System.out.println("Stopping recognition.");
        } catch (Exception e) {
            System.out.println("Unable to create LiveSpeechRecognizer");
            System.exit(1);
        }
        //comment above and uncomment below to enter input from keyboard, not mic
  /*      Scanner scan = new Scanner(System.in);
        while (true) {
            h.hear(scan.next().toLowerCase());
        }*/
    }

    public void setHearable(Hearable h) {
        this.h = h;
    }
    public void setWord(String word) {
        this.word = word;
    }
}