package bm;

/* Mateusz Bulak */
@FunctionalInterface
public interface Voiceable {
  void voice(String message);
}
