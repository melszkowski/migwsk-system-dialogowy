package bm;

import bm.impl.*;
import javafx.application.Platform;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static bm.impl.Utils.isNumber;
import static java.util.Map.*;

/*Mateusz Bulak*/
@SuppressWarnings({"WeakerAccess", "unchecked"})
public final class Driver {

  private static final class KeyAction {
    public final String key;
    public final Runnable action;

    public KeyAction(String key, Runnable action) {
      this.key = key;
      this.action = action;
    }
  }

  //<editor-fold desc="fields">
  @Nullable
  private View view;

  boolean isViewClosed = false;

  @NotNull
  private final Voiceable voiceable;

  @Nullable
  private Future intTask;

  @Nullable
  private Future keywordTask;

  @NotNull
  private final ScheduledExecutorService timer =
   Executors.newScheduledThreadPool(1);

  @NotNull
  private final StringIntegerBuilder siBuilder = new StringIntegerBuilder();

  @NotNull
  private final ArrayList<String> expr = new ArrayList<>();

  @NotNull
  private final Solver solver;

  // todo dot should be fixed to be "." else big decimal result should be changed

  @NotNull
  private final Map<String, String> wordNumeralMap = ofEntries(
   entry("zero", "0"),
   entry("one", "1"),
   entry("two", "2"),
   entry("three", "3"),
   entry("four", "4"),
   entry("five", "5"),
   entry("six", "6"),
   entry("seven", "7"),
   entry("eight", "8"),
   entry("nine", "9"),
   entry("ten", "10"),
   entry("eleven", "11"),
   entry("twelve", "12"),
   entry("thirteen", "13"),
   entry("fourteen", "14"),
   entry("fifteen", "15"),
   entry("sixteen", "16"),
   entry("seventeen", "17"),
   entry("eighteen", "18"),
   entry("nineteen", "19"),
   entry("twenty", "20"),
   entry("thirty", "30"),
   entry("forty", "40"),
   entry("fifty", "50"),
   entry("sixty", "60"),
   entry("seventy", "70"),
   entry("eighty", "80"),
   entry("ninety", "90"),
   entry("hundred", "100"),
   entry("thousand", "1000"),
   entry("million", "1000000"),
   entry("billion", "1000000000")
  );
  @NotNull
  private final Map<String, String> numeralWordMap;
  {
    String[] values = wordNumeralMap.keySet().toArray(new String[]{});
    @SuppressWarnings("unchecked")
    Entry<String, String>[] entries = new Entry[values.length];
    for (int i = 0; i < values.length; i++) {
      String value = values[i];
      entries[i] = entry(wordNumeralMap.get(value), value);
    }
    numeralWordMap = Map.ofEntries(entries);
  }

  // todo dont allow to press when will produce error e.g. preop after number
  // todo first zero and then op should work
  // todo change bigdecimal to string to plain string

  private final Map<String, Set<String>> keyChainMap;

  private final Map<String, Runnable> keyActionMap;

  private final StringBuilder keyBuff = new StringBuilder();
  //</editor-fold>

  Driver(@NotNull Config conf) {
    this.solver = new Solver(conf);
    this.voiceable = conf.voiceable;

    //<editor-fold desc="keyChainMap & keyActionMap">

    List<KeyAction> keyActions = new ArrayList<>();

    if (conf.useParentheses) {
      Runnable la = () -> {
        expr.add(Config.lpRep);
        if (view != null) view.pressAppendable(Config.lpRep);
      };
      for (String key : conf.lpKeys)
        keyActions.add(new KeyAction(key, la));

      Runnable ra = () -> {
        expr.add(Config.rpRep);
        if (view != null) view.pressAppendable(Config.rpRep);
      };
      for (String key : conf.rpKeys)
        keyActions.add(new KeyAction(key, ra));
    }

    keyActions.add(new KeyAction("point", () -> {
       boolean endsWithNum = false;
      for (int i = expr.size() - 1; i >= 0; i--) {
        String el = expr.get(i);
        if (isNumber(el)) {
          if (el.indexOf(Config.dotRepC) != -1) return;
          endsWithNum = true;
        } else {
          if (Config.dotRepS.equals(el)) return;
          else break;
        }
      }

      if (!endsWithNum) {
        expr.add("0");
        if (view != null) view.pressAppendable("0");
      }
      expr.add(Config.dotRepS);
      if (view != null) view.pressAppendable(Config.dotRepS);
    }));

    keyActions.add(new KeyAction("delete", () -> {
      int len = 0;
      if (!expr.isEmpty()) len = expr.remove(expr.size() - 1).length();
      if (view != null) view.pressBsc(len);
    }));

    keyActions.add(new KeyAction("clear", () -> {
      expr.clear();
      if (view != null) view.pressClear();
    }));

    keyActions.add(new KeyAction("enter", () -> {
      if (view != null) view.pressEnter();
      Solution sol = solver.solve(expr.toArray(new String[expr.size()]));
      if (sol.value != null) {
        String tmp = sol.value.toPlainString();
        expr.clear();
        expr.add(tmp);
        if (view != null) view.setLabel(tmp);
        voiceable.voice(WordBuilder.numToWord(tmp));
      } else {
        if (sol.cause != null)
          sol.cause.printStackTrace();
        voiceable.voice(sol.err == null ? "failed to solve" : sol.err);
      }
    }));

    //todo 1.0000000 as result should be just 1

    keyActions.add(new KeyAction("show", this::show));

    keyActions.add(new KeyAction("close", this::close));

    keyActions.add(new KeyAction("exit", () -> {
      timer.shutdown();
      Platform.exit();
    }));

    keyActions.add(new KeyAction("and", () -> {
    }));

    for (ExprInf<? extends Expr> info : conf.getExprList()) {

      if (info.keywords.length == 0 || info.keywords[0].length() == 0)
        throw new IllegalArgumentException("keywords cannot be emtpy");

      if (isNumber(info.rep))
        throw new IllegalArgumentException("representation cannot be a number");

      Runnable action = () -> {
        expr.add(info.rep);
        if (view != null) view.pressAppendable(info.rep);
      };

      for (String multiKey : info.keywords) {
        keyActions.add(new KeyAction(multiKey, action));
      }
    }

    System.out.println("Available keywords:");
    for (KeyAction ka : keyActions)
      System.out.println("[" + ka.key + "]");

    Map<String, Runnable> keyActionMap = new HashMap<>();

    for (KeyAction ka : keyActions) {
      if (keyActionMap.get(ka.key) != null) {
        throw new IllegalArgumentException("duplicate keys");
      }
      keyActionMap.put(ka.key, ka.action);
    }

    Map<String, Set<String>> keyChainMap = new HashMap<>();

    for (KeyAction ka : keyActions) {
      String[] split = ka.key.split(" ");

      int len = split.length;

      String[] joined = Arrays.copyOf(split, len);

      for (int i = 1; i < len; i++) {
        joined[i] = joined[i - 1] + " " + joined[i];

        Set<String> existing = keyChainMap.get(joined[i - 1]);
        if (existing == null) existing = new HashSet<>();
        existing.add(split[i]);
        keyChainMap.put(joined[i - 1], existing);
      }
      Set<String> exisiting = keyChainMap.get(joined[len - 1]);
      if (exisiting == null) exisiting = new HashSet<>();
      exisiting.add("");
      keyChainMap.put(joined[len - 1], exisiting);
    }

    List<Entry<String, Runnable>> entriesA = new ArrayList<>();
    for (String key : keyActionMap.keySet())
      entriesA.add(Map.entry(key, keyActionMap.get(key)));

    this.keyActionMap = Map.ofEntries(entriesA.toArray(new Entry[entriesA.size()]));

    List<Entry<String, Set<String>>> entriesC = new ArrayList<>();
    for (String key : keyChainMap.keySet()) {
      Set<String> tmp = keyChainMap.get(key);
      entriesC.add(Map.entry(key, Set.of(
       tmp.toArray(new String[tmp.size()]))));
    }

    this.keyChainMap = Map.ofEntries(entriesC.toArray(new Entry[entriesC.size()]));

    //</editor-fold>

    if (conf.constructView) {
      new Thread(() -> View.launch(View.class)).start();
      view = View.awaitAndGetInstance();
      view.configure(conf);
      Platform.setImplicitExit(false);
    }
  }

  // <editor-fold desc="public">
  public void show() {
    if (view == null) {
      voiceable.voice("The view has not been constructed.");
    } else {
      isViewClosed = false;
      view.show();
    }
  }

  public void close() {
    if (view == null) {
      voiceable.voice("The view has not been constructed.");
    } else {
      if (isViewClosed) {
        voiceable.voice("The view has already been closed.");
      } else {
        isViewClosed = true;
        view.close();
      }
    }
  }

  public void accept(@NotNull String... input) {
    for (String s : input)
      for (String ss : s.split(" "))
        accept_(ss);
  }
  // </editor-fold>

  // todo digitalized display

  // <editor-fold desc="private">

  private void accept_(@NotNull String in) {
    if (in.isEmpty()) {
      return;
    }

    stopIntTask();
    stopKeywordTask();
    String maybe = wordNumeralMap.get(in);
    if (maybe != null) {
      if (keyBuff.length() == 0) {
        siBuilder.put(maybe);
        startIntTask();
      } else {
        /* a keyword is being built */
        String buffStr = keyBuff.toString();
        Set<String> conn = keyChainMap.get(buffStr);
        if (conn.contains(in)) {
          /* it is a keyword continuation */
          keyBuff.append(' ');
          keyBuff.append(in);
          conn = keyChainMap.get(buffStr);
          /* not null; in wouldn't be contained */
          if (conn.contains("")) startKeywordTask(buffStr);
          /* else no op; wait */
        } else {
          /* so it is a number */
          finalizeKeyword(buffStr);

          siBuilder.put(maybe);
          startIntTask();
        }
      }
    } else {
      finalizeInt();

      if (keyBuff.length() == 0) {
        Set<String> conn = keyChainMap.get(in);
        if (conn == null) voiceable.voice(in + " is not a recognized keyword");
        else {
          keyBuff.append(in);
          if (conn.contains("")) {
            if (conn.size() == 1) finalizeKeyword(in);
            else stopKeywordTask();
          }
        }
      } else {
        String buffStr = keyBuff.toString();
        Set<String> conn = keyChainMap.get(buffStr);
        /* not null; buff would be empty */
        if (conn.contains(in)) {
          keyBuff.append(' ');
          keyBuff.append(in);
          buffStr = keyBuff.toString();
          conn = keyChainMap.get(buffStr);
          /* not null; in wouldn't be contained */
          if (conn.contains("")) startKeywordTask(buffStr);
          /* else no op; wait */
        } else {
          /* push and start over */
          finalizeKeyword(buffStr);

          keyBuff.delete(0, keyBuff.length());
          conn = keyChainMap.get(in);
          if (conn == null) voiceable.voice(in + " is not a recognized keyword");
          else {
            if (conn.size() == 1 && conn.contains(""))
              finalizeKeyword(in);
            else {
              keyBuff.append(in);
              if (conn.contains("")) startKeywordTask(in);
              /* else no op; wait */
            }
          }
        }
      } //todo display commands but aliases in one line
    } // todo dot eats dot
    // todo make dot optional but hardcode key and symbol
  }

  //<editor-fold desc="tasks">
  private void startIntTask() {
    this.intTask = timer.schedule(() -> {
      intTask = null;
      finalizeInt();
    }, 500, TimeUnit.MILLISECONDS);
  }

  private void stopIntTask() {
    Future intTask = this.intTask;
    if (intTask != null) intTask.cancel(false);
    this.intTask = null;
  }

  private void finalizeInt() {
    if (!siBuilder.isEmpty()) {
      String si = siBuilder.fuseAndGet();
      siBuilder.clear();
      expr.add(si);

      if (view != null) {

        char[] digs = si.toCharArray();
        String[] reps = new String[digs.length];
        for (int i = 0; i < digs.length; i++)
          reps[i] = String.valueOf(digs[i]);

        view.pressAppendable(reps);
      }
    }
  }

  private void startKeywordTask(String keyword) {
    this.keywordTask = timer.schedule(
     () -> finalizeKeyword(keyword), 500, TimeUnit.MILLISECONDS);
  }

  private void stopKeywordTask() {
    Future keywordTask = this.keywordTask;
    if (keywordTask != null) keywordTask.cancel(false);
    this.keywordTask = null;
  }

  private void finalizeKeyword(String keyword) {
    keyBuff.delete(0, keyBuff.length());
    Runnable tmp = keyActionMap.get(keyword);
    if (tmp == null) voiceable.voice(keyword + " is not a recognized command.");
    else tmp.run();
  }
  //</editor-fold>

  // todo add static final constants to config class


  //todo symbols cannot be a dot
  // todo restrict representation symbols that are used for clear backspace and config
  // </editor-fold>
}
