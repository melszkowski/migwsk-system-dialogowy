package bm;

import bm.impl.Config;
import bm.impl.ExprInf;
import bm.op.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/* Mateusz Bulak */
@SuppressWarnings("WeakerAccess")
public final class DriverFactory {

  private final Config conf = new Config();
  private List<ExprInf<BinOp>> bindOpsBuff = new ArrayList<>();

  //<editor-fold desc="add expr">
  public DriverFactory addConst(Const constant, String rep, int posX, int posY,
                                String... keywords) {
    return addConst(constant, rep, posX, posY, false, keywords);
  }

  public DriverFactory addConst(Const constant, String rep, int posX, int posY,
                                boolean accent, String... keywords) {
    notNull(constant, rep, keywords);
    notNull((Object[]) keywords);
    conf.constants.add(new ExprInf<>(constant, rep, posX, posY, accent, keywords));
    return this;
  }

  public DriverFactory addPostOp(PostOp po, String rep, int posX, int posY,
                                 String... keywords) {
    return addPostOp(po, rep, posX, posY, false, keywords);
  }

  public DriverFactory addPostOp(PostOp po, String rep, int posX, int posY,
                                 boolean accent, String... keywords) {
    notNull(po, rep, keywords);
    notNull((Object[]) keywords);
    conf.postOps.add(new ExprInf<>(po, rep, posX, posY, accent, keywords));
    return this;
  }

  public DriverFactory addPreOp(PreOp po, String rep, int posX, int posY,
                                String... keywords) {
    return addPreOp(po, rep, posX, posY, false, keywords);
  }

  public DriverFactory addPreOp(PreOp po, String rep, int posX, int posY,
                                boolean accent, String... keywords) {
    notNull(po, rep, keywords);
    notNull((Object[]) keywords);
    conf.preOps.add(new ExprInf<>(po, rep, posX, posY, accent, keywords));
    return this;
  }

  public DriverFactory addBinOp(BinOp bo, String rep, int posX, int posY,
                                String... keywords) {
    return addBinOp(bo, rep, posX, posY, false, keywords);
  }

  public DriverFactory addBinOp(BinOp bo, String rep, int posX, int posY,
                                boolean accent, String... keywords) {
    notNull(bo, rep, keywords);
    notNull((Object[]) keywords);
    bindOpsBuff.add(new ExprInf<>(bo, rep, posX, posY, accent, keywords));
    return this;
  }
  //</editor-fold>

  public DriverFactory lowerBinOpPrecedence() {
    pushBinOps();
    return this;
  }

  private void pushBinOps() {
    if (!bindOpsBuff.isEmpty()) {
      conf.binOps.add(bindOpsBuff);
      bindOpsBuff = new ArrayList<>();
    }
  }

  public DriverFactory setVoiceable(Voiceable v) {
    notNull(v);
    conf.voiceable = v;
    return this;
  }

  public DriverFactory setConstructView(boolean b) {
    conf.constructView = b;
    return this;
  }

  public DriverFactory setDigPos(int dig, int x, int y) {
    if (dig < 0 || dig > 9) {
      throw new IllegalArgumentException("A DIGIT is required.");
    }
    conf.digX[dig] = x;
    conf.digY[dig] = y;
    return this;
  }

  public DriverFactory setDotPos(int x, int y) {
    conf.dotPosX = x;
    conf.dotPosY = y;
    return this;
  }

  public DriverFactory setClearPos(int x, int y) {
    conf.clearPosX = x;
    conf.clearPosY = y;
    return this;
  }

  public DriverFactory setBspPos(int x, int y) {
    conf.bspPosX = x;
    conf.bspPosY = y;
    return this;
  }

  public DriverFactory setEnterPos(int x, int y) {
    conf.enterPosX = x;
    conf.enterPosY = y;
    return this;
  }

  public DriverFactory setUseParentheses(boolean b) {
    conf.useParentheses = b;
    return this;
  }

  public DriverFactory setLeftParenthesis(int x, int y, String... keywords) {
    conf.lpPosX = x;
    conf.lpPosY = y;
    conf.lpKeys = keywords;
    return this;
  }

  public DriverFactory setRightParenthesis(int x, int y, String... keywords) {
    conf.rpPosX = x;
    conf.rpPosY = y;
    conf.rpKeys = keywords;
    return this;
  }

  public Driver construct() {
    pushBinOps();
    conf.validate();
    return new Driver(conf);
  }

  public static DriverFactory constructDefault() {
    return new DriverFactory()

        .addPreOp(right -> right.add(new BigDecimal("15")), "fun", 0, 0, "fun")
        .addPreOp(new Sine(), "sin", 1, 0, "sine")
        .addConst(new Pi(), "pi", 0, 1, "pi")
        .addPostOp(new Factorial(), "!", 0, 2, "factorial")
        .addPreOp(new Negation(), "-", 0, 3, "negative")

        .addBinOp(new Division(), " \u00F7 ", 4, 0, "divided by")
        .addBinOp(new Multiplication(), " * ", 4, 1, "times")
        .lowerBinOpPrecedence()
        .addBinOp(new Subtraction(), " - ", 4, 2, "minus")
        .addBinOp(new Addition(), " + ", 4, 3, "plus")

        .setDigPos(7, 1, 1)
        .setDigPos(8, 2, 1)
        .setDigPos(9, 3, 1)

        .setDigPos(4, 1, 2)
        .setDigPos(5, 2, 2)
        .setDigPos(6, 3, 2)

        .setDigPos(1, 1, 3)
        .setDigPos(2, 2, 3)
        .setDigPos(3, 3, 3)

        .setDigPos(0, 2, 4)

        .setDotPos(3, 4)

        .setLeftParenthesis(0, 4, "open parenthesis")
        .setRightParenthesis(1, 4, "close parenthesis")

        .setEnterPos(4, 4)

        .setClearPos(2, 0)
        .setBspPos(3, 0);
  }

  private void notNull(Object... os) {
    for (Object o : os)
      if (o == null) throw new NullPointerException();
  }

  public static void main(String[] args) {

    DriverFactory df = DriverFactory.constructDefault();
    //df.setVoiceable(System.out::println);

    Driver d = df.construct();
    d.show();

    Scanner sc = new Scanner(System.in);
    while (true) {
      String in = sc.nextLine();
      d.accept(in); //!!
      if ("exit".equals(in)) break;
    }
  }
}
