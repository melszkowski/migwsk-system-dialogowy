package bm.impl;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.Region;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.util.Map.entry;

@SuppressWarnings({"WeakerAccess", "unchecked"})
public final class ButtonPad extends Region {

  private static final class BtnInf {
    public final String txt;
    public final int x, y;
    public final boolean accent;

    public BtnInf(String txt, int x, int y, boolean accent) {
      this.txt = txt;
      this.x = x;
      this.y = y;
      this.accent = accent;
    }
  }

  private double lastBtnW, lastBtnH;
  private Map<String, Button> repButtonMap;
  private Button[][] buttons;
  private int columns, rows;
  private double prefW, prefH;

  public void configure(Config conf) {
    List<BtnInf> infos = new ArrayList<>();

    for (int i = 0; i < 10; i++)
      infos.add(new BtnInf(Integer.toString(i), conf.digX[i], conf.digY[i], true));

    for (ExprInf<? extends Expr> info : conf.getExprList())
      infos.add(new BtnInf(info.rep, info.x, info.y, false));

    infos.add(new BtnInf(Config.clearRep, conf.clearPosX, conf.clearPosY, false));
    infos.add(new BtnInf(Config.bspRep, conf.bspPosX, conf.bspPosY, false));
    infos.add(new BtnInf(Config.enterRep, conf.enterPosX, conf.enterPosY, false));
    infos.add(new BtnInf(Config.dotRepS, conf.dotPosX, conf.dotPosY, true));
    if (conf.useParentheses) {
      infos.add(new BtnInf(Config.lpRep, conf.lpPosX, conf.lpPosY, false));
      infos.add(new BtnInf(Config.rpRep, conf.rpPosX, conf.rpPosY, false));
    }

    int
        minX = infos.get(0).x,
        maxX = minX,
        minY = infos.get(0).y,
        maxY = minY;
    for (BtnInf info : infos) {
      minX = min(minX, info.x);
      maxX = max(maxX, info.x);
      minY = min(minY, info.y);
      maxY = max(maxY, info.y);
    }

    columns = maxY - minY + 1;
    rows = maxX - minX + 1;
    Button[][] btns = new Button[columns][rows];

    List<Entry<String, Button>> repButtons = new ArrayList<>();

    for (BtnInf info : infos) {
      if (btns[info.y][info.x] != null) {
        throw new IllegalArgumentException(
            info.txt + " button covers " + btns[info.y][info.x].getText());
      }
      Button btn = new Button(info.txt, info.accent);
      btns[info.y][info.x] = btn;
      repButtons.add(entry(info.txt, btn));
    }

    repButtonMap = Map.ofEntries(
        repButtons.toArray(new Entry[repButtons.size()]));

    buttons = btns;
    ObservableList<Node> children = getChildren();
    for (Button[] row : btns)
      for (Button b : row)
        if (b != null)
          children.add(b);

    double
        maxMinW = 0,
        maxMinH = 0,
        maxPrefW = 0,
        maxPrefH = 0;
    for (Button[] bs : buttons)
      for (Button b : bs) {
        maxMinW = max(b.minWidth(-1), maxMinW);
        maxMinH = max(b.minHeight(-1), maxMinH);
        maxPrefW = max(b.prefWidth(-1), prefW);
        maxPrefH = max(b.prefHeight(-1), prefH);
      }
    prefW = (maxPrefW + 8) * columns;
    prefH = (maxPrefH + 8) * rows;
  }

  public void arm(@NotNull String rep) {
    repButtonMap.get(rep).arm();
  }

  public void disarm(@NotNull String rep) {
    repButtonMap.get(rep).disarm();
  }

  @Override
  protected double computeMinWidth(double h) {
    return 0;
  }

  @Override
  protected double computePrefWidth(double h) {
    return prefW;
  }

  @Override
  protected double computeMinHeight(double w) {
    return 0;
  }

  @Override
  protected double computePrefHeight(double w) {
    return prefH;
  }

  @Override
  protected void layoutChildren() {
    if (buttons == null || repButtonMap == null) {
      return;
    }
    double w = getWidth();
    double h = getHeight();

    double marg = min(h / rows * 0x1p-4, w / columns * 0x1p-4);
    double marg2 = marg * 2;

    double btnW = (w - columns * marg2) / columns;
    double btnH = (h - rows * marg2) / rows;

    if (lastBtnW != btnW || lastBtnH != btnH) {
      lastBtnW = btnW;
      lastBtnH = btnH;
      for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
          Button b = buttons[i][j];
          if (b == null) continue;
          b.resize(btnW, btnH);
          b.setLayoutX(j * (btnW + marg2) + marg);
          b.setLayoutY(i * (btnH + marg2) + marg);
        }
      }
    }
  }
}
