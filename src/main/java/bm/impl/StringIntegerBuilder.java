package bm.impl;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Scanner;

/* Mateusz Bulak */
@SuppressWarnings("WeakerAccess")
public final class StringIntegerBuilder {

  @NotNull
  private ArrayList<String> buff = new ArrayList<>();

  @NotNull
  private final Object lock = new Object();

  public void clear() {
    synchronized (lock) {
      buff.clear();
    }
  }

  /**
   * The input string can only contain digits.
   * Argument validation is not performed for speed
   * and the behavior is unspecified for invalid arguments.
   */
  public void put(@NotNull String input) {
    synchronized (lock) {
      buff.add(input);
    }
  }

  public boolean isEmpty() {
    return buff.isEmpty();
  }

  /**
   * e.g. 22, 1111 will result in 22111 but neither is expected to be an argument.
   * Not including that case for speed.
   * <p>
   * Doesn't remove leading zeros. The display code should do that.
   */
  @NotNull
  public String fuseAndGet() {
    String[] elements;
    synchronized (lock) {
      elements = buff.toArray(new String[0]);
    }

    StringBuilder result = new StringBuilder();
    StringBuilder subResult = new StringBuilder();
    int lastModLen = 0;
    int longestModLen = 0;

    for (int i = elements.length - 1; i >= 0; i--) {
      String el = elements[i];
      int elLen = el.length();

      if (el.charAt(0) == '0') {
        /* zero start is special */
        subResult.insert(0, el);
        result.insert(0, subResult);
        subResult = new StringBuilder();
        lastModLen = 0;
        longestModLen = 0;
      } else if (lastModLen == 0) {
        /* check if first subinput */
        if (subResult.length() == 0) {
          subResult.append(el);
        } else {
          /* check if can merge */
          if (getFollowingZerosCount(el) >= subResult.length()) {
            subResult.insert(0, el.substring(0, elLen - subResult.length()));
          } else {
            /* cannot merge then maybe can modify */
            if (subResult.charAt(0) == '1'
                && subResult.length() > 2
                && subResult.length() > elLen) {

              /* fist modifier locally */
              subResult.deleteCharAt(0);
              subResult.insert(0, el);
              longestModLen = elLen;
              lastModLen = elLen;
            } else {
              /* cannot modify or merge */
              result.insert(0, subResult);
              subResult = new StringBuilder(el);
            }
          }
        }
      } else {
        int followingZerosCount = getFollowingZerosCount(el);

        if (elLen < subResult.length()
            && elLen > longestModLen
            && followingZerosCount >= longestModLen) {

          /* merge modifiers - big merge */
          subResult.insert(0, el.substring(0, elLen - lastModLen));
          longestModLen = elLen;
          lastModLen = elLen;
        } else if (elLen < subResult.length()
            && elLen > lastModLen
            && elLen < longestModLen
            && followingZerosCount >= lastModLen) {

          /* merge modifiers - small merge */
          subResult.insert(0, el.substring(0, elLen - lastModLen));
          lastModLen = elLen;
        } else if (subResult.charAt(0) == '1'
            && lastModLen > 2
            && lastModLen > elLen) {

          /* modify modifier */
          subResult.deleteCharAt(0);
          subResult.insert(0, el);
          lastModLen = elLen;
        } else {
          longestModLen = 0;
          lastModLen = 0;

          if (followingZerosCount >= subResult.length()) {
            /* merge whole number */
            subResult.insert(0, el.substring(0, elLen - subResult.length()));
          } else {
            /* cannot modify or merge */
            result.insert(0, subResult);
            subResult = new StringBuilder(el);
          }
        }
      }
    }

    if (subResult.length() != 0) {
      result.insert(0, subResult);
    }

    return result.toString();
  }

  private static int getFollowingZerosCount(@NotNull String num) {
    int res = 0;
    for (int j = num.length() - 1; j > 0; j--) {
      /* > 0 because following zeros */
      if (num.charAt(j) == '0') {
        res++;
      } else {
        break;
      }
    }
    return res;
  }

  public static void main(String[] args) {
    StringIntegerBuilder bib = new StringIntegerBuilder();
    System.out.println("input text here");
    Scanner sc = new Scanner(System.in);
    while (true) {
      String line = sc.nextLine();
      if ("exit".equals(line)) break;
      else if ("do".equals(line)) {
        System.out.println(bib.fuseAndGet());
        bib.clear();
      } else {
        bib.put(line);
      }
    }
    sc.close();
  }
}
