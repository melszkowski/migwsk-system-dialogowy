package bm.impl;

/* Mateusz Bulak */
public final class ExprInf<T extends Expr> {

  public final T expr;
  public final String rep;
  public final int x;
  public final int y;
  public final String[] keywords;
  public final boolean accent;

  public ExprInf(T expr, String rep, int x, int y, boolean accent, String... keywords) {
    this.expr = expr;
    this.rep = rep;
    this.x = x;
    this.y = y;
    this.accent = accent;
    this.keywords = keywords;
  }
}
