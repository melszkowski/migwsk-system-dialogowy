package bm.impl;

import javafx.animation.PauseTransition;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

/* Mateusz Bulak */
@SuppressWarnings("WeakerAccess")
public final class View extends Application {

  //<editor-fold desc="fields">
  private static CountDownLatch latch = new CountDownLatch(1);
  private static View instance;
  private Stage stage;
  private final Label exprLabel;
  private final ButtonPad btnPad;

  private boolean isClosed = true;

  private final ConcurrentLinkedQueue<Runnable> actionQueue = new ConcurrentLinkedQueue<>();
  private final AtomicBoolean isAnimatingPress = new AtomicBoolean();

  private final StringBuilder exprBuff = new StringBuilder();
  //</editor-fold>

  public View() {
    View.instance = this;
    exprLabel = new Label();
    btnPad = new ButtonPad();
  }

  public static View awaitAndGetInstance() {
    try {
      latch.await();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return View.instance;
  }

  //<editor-fold desc="public">
  public void show() {
    if (isClosed) {
      isClosed = false;

      Platform.runLater(() -> {
        stage = new Stage();
        setOnCloseRequest(stage);
        start(stage);
        stage.show();
        stage.toFront();
      });
    } else {
      Platform.runLater(stage::show);
    }
  }

  public void close() {
    if (stage != null) {
      Platform.runLater(stage::close);
    }
  }

  public void setLabel(String txt) {
    actionQueue.add(() -> {
      exprBuff.delete(0, exprBuff.length());
      exprBuff.append(txt);
      updateExprLabel();
      Runnable another = actionQueue.poll();
      if (another != null) {
        another.run();
      } else {
        isAnimatingPress.set(false);
      }
    });
    runAction();
  }

  public void pressAppendable(String... reps) {
    for (String rep : reps) {
      actionQueue.add(getPressAnimatin(rep, () -> {
        exprBuff.append(rep);
        updateExprLabel();
      }));
    }
    runAction();
  }

  public void pressBsc(int chrToBeDeleted) {
    actionQueue.add(getPressAnimatin(Config.bspRep, () -> {
      int len = exprBuff.length();
      int from = len - chrToBeDeleted;
      if (from < 0) from = 0;
      exprBuff.delete(from, len);
      updateExprLabel();
    }));
    runAction();
  }

  public void pressClear() {
    actionQueue.add(getPressAnimatin("C", () -> {
      exprBuff.delete(0, exprBuff.length());
      updateExprLabel();
    }));
    runAction();
  }

  public void pressEnter() {
    actionQueue.add(getPressAnimatin("=", () -> {
      // no op
    }));
    runAction();
  }
  //</editor-fold>

  @Override
  public void start(Stage stage) {
    this.stage = stage;
    setOnCloseRequest(stage);

    updateExprLabel();

    Region root = new Region() {
      {
        getChildren().addAll(exprLabel, btnPad);
      }

      @Override
      protected void layoutChildren() {
        double w = getWidth(), h = getHeight();
        double exprPrefH = exprLabel.prefHeight(w);
        exprLabel.resize(w, exprPrefH);

        btnPad.setLayoutY(exprPrefH);
        btnPad.resize(w, h - exprPrefH);
      }
    };

    Scene scene = new Scene(root, 400, 400);
    scene.getStylesheets().add(View.class.getResource("/style.css").toExternalForm());
    scene.addEventFilter(MouseEvent.ANY, Event::consume);
    stage.setScene(scene);
    stage.setTitle("David");
    latch.countDown();
  }

  public void configure(Config conf) {
    btnPad.configure(conf);
  }

  //<editor-fold desc="private">
  private void updateExprLabel() {
    String result;
// todo what about representation with 3 digits or more
// todo no dot allowed in other symbols

    if (exprBuff.length() == 0) {
      result = "0";
    } else {
      final StringBuilder sb = new StringBuilder(exprBuff.length());
      final int dotInd = exprBuff.indexOf(Config.dotRepS);

      int consec = 0;
      for (int i = (dotInd == -1 ? exprBuff.length() : dotInd) - 1; i >= 0; i--) {
        char c = exprBuff.charAt(i);
        sb.append(c);
        if (c >= '0' && c <= '9') {
          if (consec == 2) {
            consec = 0;
            sb.append(' ');
          } else {
            consec++;
          }
        } else {
          consec = 0;
        }
      }
      if (sb.charAt(sb.length() - 1) == ' ') sb.deleteCharAt(sb.length() - 1);
      sb.reverse();
      if (dotInd != -1) sb.append(exprBuff, dotInd, exprBuff.length());
      result = sb.toString();
    }
    exprLabel.setText(result);
  }

  private void runAction() {
    if (!isAnimatingPress.getPlain()) {
      Runnable r = actionQueue.poll();
      if (r != null) {
        isAnimatingPress.set(true);
        Platform.runLater(r);
      }
    }
  }

  private Runnable getPressAnimatin(String rep, Runnable action) {
    return () -> {
      PauseTransition p1 = new PauseTransition(Duration.millis(50));
      p1.setOnFinished(ae -> {
        btnPad.arm(rep);
        action.run();
      });
      p1.play();
      PauseTransition p2 = new PauseTransition(Duration.millis(175));
      p2.setOnFinished(ae -> {
        btnPad.disarm(rep);
        Runnable another = actionQueue.poll();
        if (another != null) {
          another.run();
        } else {
          isAnimatingPress.set(false);
        }
      });
      p2.play();
    };
  }

  private void setOnCloseRequest(Stage stage) {
    stage.setOnCloseRequest(we -> {
      View.this.isClosed = true;
      View.this.stage = null;
    });
  }

  // todo reserved keywords C, =, \u232B

  //</editor-fold>
}
