package bm.impl;

/* Mateusz Bulak */
public final class WordBuilder {

  private static String[]
   digits = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"},
   teens = {"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"},
   tys = {"", "ten", "twenty", "thirty", "forty", "fify", "sixty", "seventy", "eighty", "ninety"},
   pows = {"", "thousand", "million", "billion"};

  private static void appendSubThousand(StringBuilder sb, char a, char b, char c) {
    a -= '0';
    b -= '0';
    c -= '0';

    if (a != 0) {
      sb.append(digits[a]);
      sb.append(" hundred ");
    }
    boolean teen = false;
    if (b != 0) {
      if (b == 1) teen = true;
      else {
        sb.append(tys[b]);
        sb.append(' ');
      }
    }

    if (teen) {
      sb.append(teens[c]);
      sb.append(' ');
    } else if (c != 0) {
      sb.append(digits[c]);
      sb.append(' ');
    }
  }

  public static String numToWord(String number) {
    StringBuilder sb = new StringBuilder();

    char[] chars = number.toCharArray();
    int dotInd = -1;
    for (int i = 0; i < chars.length; i++) {
      if (chars[i] == '.') {
        dotInd = i;
        break;
      }
    }

    int shift = 0;
    if (chars[0] == '-') {
      sb.append("negative");
      sb.append(' ');
      shift = 1;
    }

    //<editor-fold desc="integer part">
    int intLen = (dotInd == -1 ? chars.length : dotInd) - shift;
    int trim = intLen % 3;
    if (trim == 0) {
      appendSubThousand(sb, chars[shift], chars[shift + 1], chars[shift + 2]);
    } else if (trim == 2) {
      appendSubThousand(sb, '0', chars[shift], chars[shift + 1]);
    } else if (trim == 1) {
      appendSubThousand(sb, '0', '0', chars[shift]);
    }

    boolean appendPow = true;
    boolean lastWasOOR = false;

    for (int i = (trim == 0 ? 3 : trim) + shift, end = intLen + shift; i < end; i += 3) {
      int left = end - i;

      char a, b, c;

      switch (left) {
        case 1:
          a = '0';
          b = '0';
          c = chars[i];
          break;
        case 2:
          a = '0';
          b = chars[i];
          c = chars[i + 1];
          break;
        default:
          a = chars[i];
          b = chars[i + 1];
          c = chars[i + 2];
          break;
      }

      int pow = left / 3;
      if (pow >= pows.length) {
        pow = 1;
        appendPow = true;
        lastWasOOR = true;
      }

      if (appendPow || lastWasOOR) {
        if (!appendPow) lastWasOOR = false;
        sb.append(pows[pow]);
        sb.append(' ');
      }

      appendSubThousand(sb, a, b, c);

      appendPow = a != '0' || b != '0' || c != '0';
    }
    //</editor-fold>

    //<editor-fold desc="float part">
    if (dotInd != -1) {
      sb.append("point");
      sb.append(' ');
      for (int i = dotInd + 1; i < chars.length; i++) {
        sb.append(digits[chars[i] - '0']);
        sb.append(' ');
      }
    }
    //</editor-fold>

    if (sb.length() == 0) {
      sb.append(digits[0]);
    } else if (sb.charAt(sb.length() - 1) == ' ') sb.deleteCharAt(sb.length() - 1);
    return sb.toString();
  }

  public static void main(String[] args) {
    System.out.println(numToWord("0"));
  }
}

