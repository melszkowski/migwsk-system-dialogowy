package bm.impl;

import javafx.css.PseudoClass;
import javafx.geometry.Bounds;
import javafx.scene.layout.Region;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import static java.lang.Math.min;

@SuppressWarnings("WeakerAccess")
public final class Button extends Region {

  private final Text txt;
  private static final PseudoClass ARMED = PseudoClass.getPseudoClass("armed");
  private static final PseudoClass ACCENT = PseudoClass.getPseudoClass("accent");
  private final double prefW;


  public Button(String txt, boolean accent) {
    this.txt = new Text(txt.trim());
    this.txt.setTextAlignment(TextAlignment.CENTER);
    getChildren().add(this.txt);
    getStyleClass().setAll("migwsk-button");
    pseudoClassStateChanged(ACCENT, accent);
    prefW = 32 * txt.length() + 8;
  }

  public String getText() {
    return txt.getText();
  }

  public void arm() {
    pseudoClassStateChanged(ARMED, true);
  }

  public void disarm() {
    pseudoClassStateChanged(ARMED, false);
  }

  @Override
  protected double computeMinWidth(double h) {
    return 0;
  }

  @Override
  protected double computePrefWidth(double h) {
    return prefW;
  }

  @Override
  protected double computeMinHeight(double w) {
    return 0;
  }

  @Override
  protected double computePrefHeight(double w) {
    return 40;
  }

  @Override
  protected void layoutChildren() {
    double w = getWidth();
    double h = getHeight();

    txt.setFont(Font.font(min(w * 0.6, h * 0.6)));

    Bounds bounds = txt.getLayoutBounds();
    txt.setLayoutX(w / 2 - bounds.getWidth() / 2);
    txt.setLayoutY(h / 2 - bounds.getHeight() / 2 + txt.getBaselineOffset());
  }
}
