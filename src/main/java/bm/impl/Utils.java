package bm.impl;

/* Mateusz Bulak */
public final class Utils {

  public static boolean isNumber(String s) {
    char[] chars = s.toCharArray();

    if (chars.length == 0) {
      return false;
    }
    if (chars.length == 1) {
      return chars[0] >= '0' && chars[0] <= '9';
    }

    /* minus or number */
    if (chars[0] != '-' && (chars[0] < '0' || chars[0] > '9')) {
      return false;
    }

    boolean wasDot = false;
    for (int i = 1; i < chars.length; i++) {
      if (chars[i] < '0' || chars[i] > '9') {
        if (chars[i] == '.') {
          if (wasDot) {
            return false;
          } else {
            wasDot = true;
          }
        } else {
          return false;
        }
      }
    }
    /* "111." is ok? */
    return true;
  }

  public static boolean isInteger(String s) {
    char[] chars = s.toCharArray();

    if (chars.length == 0) {
      return false;
    }

    if (chars.length == 1) {
      return chars[0] >= '0' && chars[0] <= '9';
    }

    if (chars[0] != '-' && (chars[0] < '0' || chars[0] > '9')) {
      return false;
    }

    for (int i = 1; i < chars.length; i++) {
      if (chars[i] < '0' || chars[i] > '9') {
        return false;
      }
    }
    return true;
  }
}
