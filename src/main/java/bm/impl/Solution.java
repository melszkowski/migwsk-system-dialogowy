package bm.impl;

import org.jetbrains.annotations.Nullable;

import java.math.BigDecimal;

/* Mateusz Bulak */
public final class Solution {

  @Nullable
  public final String err;
  @Nullable
  public final Throwable cause;
  @Nullable
  public final BigDecimal value;

  private Solution(@Nullable String err,
                   @Nullable Throwable cause,
                   @Nullable BigDecimal value) {
    this.err = err;
    this.cause = cause;
    this.value = value;
  }

  public Solution(String err) {
    this(err, null, null);
  }

  public Solution(BigDecimal value) {
    this(null, null, value);
  }

  public Solution(String err, Throwable cause) {
    this(err, cause, null);
  }
}
