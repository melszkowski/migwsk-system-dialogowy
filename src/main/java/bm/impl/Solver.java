package bm.impl;

import bm.op.BinOp;
import bm.op.Const;
import bm.op.PostOp;
import bm.op.PreOp;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import static bm.impl.Utils.isNumber;
import static java.util.Map.entry;

/* Mateusz Bulak */
public final class Solver {
  @SuppressWarnings({"unused", "WeakerAccess"})
  public static final class Exc extends RuntimeException {

    public Exc(String s) {
      super(s);
    }

    public Exc(Throwable cause) {
      super(cause);
    }

    public Exc(String message, Throwable cause) {
      super(message, cause);
    }
  }

  private final Map<String, Const> constMap;
  private final Map<String, PostOp> postOpMap;
  private final Map<String, PreOp> preOpMap;
  private final Map<String, BinOp> binOpMap;
  private final Map<String, Integer> binOpPrioMap;
  private final Map<String, String> repKeyMap;

  @SuppressWarnings("unchecked")
  public Solver(Config conf) {

    List<Entry<String, String>> repKeys = new ArrayList<>();
    List<Entry<String, Const>> constants = new ArrayList<>();
    List<Entry<String, PostOp>> postOps = new ArrayList<>();
    List<Entry<String, PreOp>> preOps = new ArrayList<>();
    List<Entry<String, BinOp>> binOps = new ArrayList<>();
    List<Entry<String, Integer>> binOpPrios = new ArrayList<>();

    for (ExprInf<Const> info : conf.constants) {
      if (info.keywords.length == 0 || info.keywords[0].length() == 0) {
        throw new IllegalArgumentException("keywords cannot be empty");
      }
      if (isNumber(info.rep)) {
        throw new IllegalArgumentException("representation cannot be a number");
      }
      repKeys.add(entry(info.rep, info.keywords[0]));
      constants.add(entry(info.rep, info.expr));
    }

    for (ExprInf<PostOp> info : conf.postOps) {
      if (info.keywords.length == 0 || info.keywords[0].length() == 0) {
        throw new IllegalArgumentException("keywords cannot be empty");
      }
      if (isNumber(info.rep)) {
        throw new IllegalArgumentException("representation cannot be a number");
      }
      repKeys.add(entry(info.rep, info.keywords[0]));
      postOps.add(entry(info.rep, info.expr));
    }

    for (ExprInf<PreOp> info : conf.preOps) {
      if (info.keywords.length == 0 || info.keywords[0].length() == 0) {
        throw new IllegalArgumentException("keywords cannot be empty");
      }
      if (isNumber(info.rep)) {
        throw new IllegalArgumentException("representation cannot be a number");
      }
      repKeys.add(entry(info.rep, info.keywords[0]));
      preOps.add(entry(info.rep, info.expr));
    }

    int priority = 0;
    for (List<ExprInf<BinOp>> level : conf.binOps) {
      for (ExprInf<BinOp> info : level) {
        if (info.keywords.length == 0 || info.keywords[0].length() == 0) {
          throw new IllegalArgumentException("keywords cannot be empty");
        }
        if (isNumber(info.rep)) {
          throw new IllegalArgumentException("representation cannot be a number");
        }
        repKeys.add(entry(info.rep, info.keywords[0]));
        binOps.add(entry(info.rep, info.expr));
        binOpPrios.add(entry(info.rep, priority));
      }
      priority++;
    }

    constMap = Map.ofEntries(constants.toArray(new Entry[constants.size()]));
    postOpMap = Map.ofEntries(postOps.toArray(new Entry[postOps.size()]));
    preOpMap = Map.ofEntries(preOps.toArray(new Entry[preOps.size()]));
    binOpMap = Map.ofEntries(binOps.toArray(new Entry[binOps.size()]));
    binOpPrioMap = Map.ofEntries(binOpPrios.toArray(new Entry[binOpPrios.size()]));
    repKeyMap = Map.ofEntries(repKeys.toArray(new Entry[repKeys.size()]));
  }

  // fixme for now elements have to be unique globally (not checked)

  private static final class Element {
    public String s;
    public BigDecimal bd;
    public Expr expr;
  }

  // todo preop rep, keyword must be unique
  // todo postop rep, keyword must be unique
  // todo const rep, keyword must be unique
  // todo binop rep, keyword must be unique
  // todo change to other representation than string cos its heavy

  public Solution solve(String[] expr) {

    /* concatenate numbers */
    boolean lastWasNumber = false;
    for (int i = 0; i < expr.length; i++) {
      if (isNumber(expr[i])) {
        if (lastWasNumber) {
          expr[i] = expr[i - 1] + expr[i];
          expr[i - 1] = null;
        } else {
          lastWasNumber = true;
        }
      } else {
        lastWasNumber = false;
      }
    }

    expr = removeNulls(expr);

    /* handle dots */
    boolean doingDot = false;
    for (int i = 0; i < expr.length; i++) {
      if (Config.dotRepS.equals(expr[i])) {
        doingDot = true;
        if (i == 0) expr[i] = "0" + Config.dotRepS;
        else {
          expr[i] = expr[i - 1] + Config.dotRepS;
          expr[i - 1] = null;
        }
      } else if (doingDot) {
        expr[i] = expr[i - 1] + expr[i];
        expr[i - 1] = null;
        doingDot = false;
      }
    }
    if (doingDot) expr[expr.length - 1] = expr[expr.length - 1] + "0";

    expr = removeNulls(expr);

    /* apply constants */
    for (int i = 0; i < expr.length; i++) {
      String e = expr[i];
      Const constant = constMap.get(e);
      if (constant != null) {
        expr[i] = constant.valueOf().toPlainString();
      }
    }

    int lastExprLen = expr.length;

    try {
      do {
        int leftInd = lpInd(expr, Config.lpRep);
        int rightInd = rpInd(expr, leftInd + 1, Config.rpRep);

        if (leftInd == -1 && rightInd != -1 || leftInd != -1 && rightInd == -1) {
          throw new Exc("Uneven number of parentheses.");
        }

        if (leftInd == -1) {
          applyPostOp(expr, 0, expr.length);
          expr = removeNulls(expr);
          applyPreOp(expr, 0, expr.length);
          expr = removeNulls(expr);
          applyBinOp(expr, 0, expr.length);
          expr = removeNulls(expr);
        } else {
        /* because starting right from left it is -1 or after */
          if (leftInd + 1 == rightInd) {
            throw new Exc("Empty parentheses.");
          }
          applyPostOp(expr, leftInd + 1, rightInd);
          expr = removeNulls(expr);

          rightInd = rpInd(expr, leftInd + 1, Config.rpRep);

          applyPreOp(expr, leftInd + 1, rightInd);
          expr = removeNulls(expr);

          rightInd = rpInd(expr, leftInd + 1, Config.rpRep);

          applyBinOp(expr, leftInd + 1, rightInd);
          expr[leftInd] = null;
          expr[rightInd] = null;
          expr = removeNulls(expr);
        }

        if (expr.length == lastExprLen) {
          throw new Exc("Missing binary operation.");
        } else {
          lastExprLen = expr.length;
        }

      } while (expr.length != 1);

    } catch (Throwable t) {
      return new Solution(t.getMessage(), t);
    }

    return new Solution(new BigDecimal(expr[0]).stripTrailingZeros());
  }

  private static int lpInd(String[] expr, String lp) {
    int ind = -1;
    for (int i = expr.length - 1; i >= 0; i--) {
      if (lp.equals(expr[i])) {
        ind = i;
        break;
      }
    }
    return ind;
  }

  private static int rpInd(String[] expr, int from, String rp) {
    int ind = -1;
    for (int i = from; i < expr.length; i++) {
      if (rp.equals(expr[i])) {
        ind = i;
        break;
      }
    }
    return ind;
  }

  private static String[] removeNulls(String[] expr) {
    int shift = 0;
    for (int i = 0; i < expr.length; i++) {
      String e = expr[i];
      if (e != null) {
        expr[i - shift] = e;
      } else {
        shift++;
      }
    }
    if (shift == 0) return expr;
    else return Arrays.copyOf(expr, expr.length - shift);
  }

  //<editor-fold desc="apply...">
  /* Following methods do not handle parentheses.
   * They assume:
   *  - no nulls in the array,
   *  - unique reps //todo could do so that instead of throwing exceptions when missing number they continue and in the end the main method checks if something is left then it throws an exception; the problem would be with description
   * */

  private void applyPostOp(String[] expr, int start, int end) {
    for (int i = start; i < end; i++) {
      String curr = expr[i];
      PostOp po = postOpMap.get(curr);
      if (po == null) continue;
      if (i == start || !isNumber(expr[i - 1])) {
        throw new Exc(
            "There is no operand for " + repKeyMap.get(curr) + " operator.");
      }
      expr[i] = po.valueOf(new BigDecimal(expr[i - 1])).stripTrailingZeros().toPlainString();
      expr[i - 1] = null;
    }
  }

  private void applyPreOp(String[] expr, int start, int end) {
    end -= 1;
    for (int i = end; i >= start; i--) {
      String curr = expr[i];
      PreOp po = preOpMap.get(curr);
      if (po == null) continue;
      if (i == end || !isNumber(expr[i + 1])) {
        throw new Exc(
            "There is no operand for " + repKeyMap.get(curr) + "operator");
      }
      expr[i] = po.valueOf(new BigDecimal(expr[i + 1])).stripTrailingZeros().toPlainString();
      expr[i + 1] = null;
    }
  }

  private void applyBinOp(String[] expr, int start, int end) {
    int[] priorities = new int[end - start];
    for (int i = start; i < end; i++) {
      Integer p = binOpPrioMap.get(expr[i]);
      priorities[i - start] = p == null ? -1 : p;
    }

    int lowest = 0;
    for (int i : priorities) {
      lowest = Math.max(i, lowest);
    }

    for (int p = 0; p <= lowest; p++) {
      for (int i = start; i < end; i++) {
        if (priorities[i - start] != p) continue;
        String curr = expr[i];
        BinOp bo = binOpMap.get(curr);
        if (bo == null) {
          throw new Exc("internal error");
        }

        //<editor-fold desc="get left">
        int leftInd = i - 1;
        String left = null;
        while (leftInd >= 0) {
          if (expr[leftInd] != null) {
            if (!isNumber(expr[leftInd])) {
              break;
            } else {
              left = expr[leftInd];
              expr[leftInd] = null;
            }
          } else {
            leftInd--;
          }
        }
        if (left == null) {
          throw new Exc(
              "There is no left operand for "
                  + repKeyMap.get(curr) + " operator");
        }
        //</editor-fold>

        //<editor-fold desc="get right">
        int rightInd = i + 1;
        String right = null;
        while (rightInd < end) {
          if (expr[rightInd] != null) {
            if (!isNumber(expr[rightInd])) {
              break;
            } else {
              right = expr[rightInd];
              expr[rightInd] = null;
            }
          } else {
            rightInd++;
          }
        }
        if (right == null) {
          throw new Exc(
              "There is no right operand for "
                  + repKeyMap.get(curr) + "operator");
        }
        //</editor-fold>

        expr[i] = bo.valueOf(
            new BigDecimal(left), new BigDecimal(right)
        ).stripTrailingZeros().toPlainString();
        /* neglecting proprities contents */
      }
    }
  }
  //</editor-fold>

  // todo dont allow other characters than letters in keywords
  // todo standarize implemented operations names
  // todo need to handle minus as preop and binop
  // todo determine precision


  // todo no number before point means implicit zero !!!!
  // fixme
  // todo scalling still sucks
}
