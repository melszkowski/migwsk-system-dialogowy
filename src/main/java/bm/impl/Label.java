package bm.impl;

import javafx.geometry.Bounds;
import javafx.scene.layout.Region;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import static java.lang.Math.min;

/* Mateusz Bulak */
@SuppressWarnings("WeakerAccess")
public final class Label extends Region {

  private final Text txt = new Text();
  private int len = 0;

  public Label() {
    txt.setTextAlignment(TextAlignment.CENTER);
    getStyleClass().setAll("migwsk-label");
    getChildren().add(txt);
  }

  public void setText(String s) {
    txt.setText(s);
    len = s.length();
    layout();
  }

  @Override
  protected double computeMinWidth(double h) {
    /* 8 + (18 * len * 4 / 3) + 8 */
    return 16 + 24 * len;
  }

  @Override
  protected double computePrefWidth(double h) {
    /* 12 + (36 * len * 4 / 3) + 12 */
    return 24 + 48 * len;
  }

  @Override
  protected double computeMinHeight(double w) {
    /* 6 + (18 * 4 / 3) + 6 */
    return 36;
  }

  @Override
  protected double computePrefHeight(double w) {
    /* 8 + (36 * 4 / 3) + 8 */
    return 64;
  }

  @Override
  protected void layoutChildren() {
    double w = getWidth();
    double h = getHeight();

    Bounds b = txt.getLayoutBounds();
    double fs = txt.getFont().getSize();

    double fw = fs * (w - 24) / b.getWidth();
    double fh = fs * (h - 16) / b.getHeight();

    txt.setFont(Font.font(min(fw, fh)));

    b = txt.getLayoutBounds();

    txt.setLayoutX(w - b.getWidth() - 12);
    txt.setLayoutY(h / 2 - b.getHeight() / 2 + txt.getBaselineOffset());

  }
}
