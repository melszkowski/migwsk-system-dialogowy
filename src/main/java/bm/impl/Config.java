package bm.impl;

import bm.Voiceable;
import bm.op.BinOp;
import bm.op.Const;
import bm.op.PostOp;
import bm.op.PreOp;

import java.util.*;

/* Mateusz Bulak */
@SuppressWarnings("WeakerAccess")
public final class Config {

  public static final char dotRepC = '.';

  public static final String
   dotRepS = String.valueOf(dotRepC),
  //todo find usaged and maybe replace with char
  clearRep = "C",
   bspRep = "\u232B",
   enterRep = "=",
   lpRep = "(",
   rpRep = ")";

  public final List<ExprInf<Const>> constants = new ArrayList<>();
  public final List<ExprInf<PostOp>> postOps = new ArrayList<>();
  public final List<ExprInf<PreOp>> preOps = new ArrayList<>();
  public final List<List<ExprInf<BinOp>>> binOps = new ArrayList<>();
  public Voiceable voiceable = s -> System.out.println("voice:" + s);

  public final Integer[]
   digX = new Integer[10],
   digY = new Integer[10];

  public Integer
   dotPosX, dotPosY,
   clearPosX, clearPosY,
   bspPosX, bspPosY,
   enterPosX, enterPosY,
   lpPosX, lpPosY,
   rpPosX, rpPosY;

  public String[]
   lpKeys,
   rpKeys;

  public boolean constructView = true;

  public boolean useParentheses = true;

  private List<ExprInf<? extends Expr>> exprList;

  public List<ExprInf<? extends Expr>> getExprList() {
    if (exprList == null) {
      List<ExprInf<? extends Expr>> exprList = new ArrayList<>();
      exprList.addAll(constants);
      exprList.addAll(postOps);
      exprList.addAll(preOps);
      for (List<ExprInf<BinOp>> sub : binOps) {
        exprList.addAll(sub);
      }
      this.exprList = exprList;
    }
    return exprList;
  }

  public void validate() {

    //todo mozna pisac to stronami, mam dosyc jak na razie

    StringBuilder errMsg = new StringBuilder();

    //<editor-fold desc="null">
    if (voiceable == null)
      errMsg.append("\nvoiceable is null");
    if (dotPosX == null)
      errMsg.append("\ndotPosX is null");
    if (dotPosY == null)
      errMsg.append("\ndotPosY is null");
    if (clearPosX == null)
      errMsg.append("\nclearPosX is null");
    if (clearPosY == null)
      errMsg.append("\nclearPosY is null");
    if (bspPosX == null)
      errMsg.append("\nbspPosX is null");
    if (bspPosY == null)
      errMsg.append("\nbspPosY is null");
    if (enterPosX == null)
      errMsg.append("\nenterPosX is null");
    if (enterPosY == null)
      errMsg.append("\nenterPosY is null");

    if (useParentheses) {
      if (lpPosX == null)
        errMsg.append("\nlpPosX is null");
      if (lpPosY == null)
        errMsg.append("\nlpPosY is null");
      if (rpPosX == null)
        errMsg.append("\nrpPosX is null");
      if (rpPosY == null)
        errMsg.append("\nrpPosY is null");

      if (lpKeys == null)
        errMsg.append("\nlpKeys is null");
      if (rpKeys == null)
        errMsg.append("\nrpKeys is null");
    }

    if (constructView) {
      for (int i = 0; i < 10; i++)
        if (digX[i] == null) {
          errMsg.append("\nposition x of digit ");
          errMsg.append(i);
          errMsg.append(" is not specified");
        }
      for (int i = 0; i < 10; i++)
        if (digY[i] == null) {
          errMsg.append("\nposition y of digit ");
          errMsg.append(i);
          errMsg.append(" is not specified");
        }
    }
    //</editor-fold>

    HashSet<Class> operationSet = new HashSet<>();

    for (ExprInf<? extends Expr> inf : getExprList()) {

      if (inf.expr == null) {
        errMsg.append("\nexpr of which keywords are ");
        errMsg.append(Arrays.toString(inf.keywords));
        errMsg.append(" is null");
        continue;
      }

      Class exprClass = inf.expr.getClass();
      if (!operationSet.add(exprClass)) {
        errMsg.append(exprClass);
        errMsg.append(" was added as operation multiple times" +
         ", which is not allowed");
      }

      if (inf.keywords == null) {
        errMsg.append("\nkeywords of ");
        errMsg.append(exprClass);
        errMsg.append(" are null");
      } else if (inf.keywords.length == 0) {
        errMsg.append("\nkeywords of ");
        errMsg.append(exprClass);
        errMsg.append(" are empty");
      }
      if (inf.rep == null) {
        errMsg.append("\nrepresentation of ");
        errMsg.append(exprClass);
        errMsg.append(" is null");
      } else if (inf.rep.length() == 0) {
        errMsg.append("\nrepresentation of ");
        errMsg.append(exprClass);
        errMsg.append(" is empty");
      } else if (inf.rep.contains(dotRepS)) {
        errMsg.append("\nrepresentation of ");
        errMsg.append(exprClass);
        errMsg.append(" contains dot representation, which is not allowed");
      }
    }

    if (errMsg.length() != 0) {
      throw new IllegalStateException(errMsg.toString());
    }
  }
}
