package bm.op;

import bm.impl.Expr;

import java.math.BigDecimal;

/* Mateusz Bulak */
@FunctionalInterface
public interface BinOp extends Expr {
  BigDecimal valueOf(BigDecimal left, BigDecimal right);
}