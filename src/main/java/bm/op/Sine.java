package bm.op;

import java.math.BigDecimal;

import static java.lang.Math.sin;

/* Mateusz Bulak */
public final class Sine implements PreOp {
  @Override
  public BigDecimal valueOf(BigDecimal right) {
    return new BigDecimal(sin(right.doubleValue()));
  }
}
