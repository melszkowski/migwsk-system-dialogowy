package bm.op;

import bm.impl.Expr;

import java.math.BigDecimal;

/* Mateusz Bulak */
@FunctionalInterface
public interface Const extends Expr {
  BigDecimal valueOf();
}