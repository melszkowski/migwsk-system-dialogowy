package bm.op;

import java.math.BigDecimal;

/* Mateusz Bulak */
public final class Negation implements PreOp {
  @Override
  public BigDecimal valueOf(BigDecimal right) {
    return right.negate();
  }
}
