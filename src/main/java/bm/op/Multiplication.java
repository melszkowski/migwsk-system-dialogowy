package bm.op;

import java.math.BigDecimal;

/* Mateusz Bulak */
public final class Multiplication implements BinOp {
  @Override
  public BigDecimal valueOf(BigDecimal left, BigDecimal right) {
    return left.multiply(right);
  }
}
