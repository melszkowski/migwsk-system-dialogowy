package bm.op;

import java.math.BigDecimal;

/* Mateusz Bulak */
public final class Subtraction implements BinOp {
  @Override
  public BigDecimal valueOf(BigDecimal left, BigDecimal right) {
    return left.subtract(right);
  }
}
