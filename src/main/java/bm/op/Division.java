package bm.op;

import bm.impl.Solver;

import java.math.BigDecimal;
import java.math.RoundingMode;

/* Mateusz Bulak */
public final class Division implements BinOp {
  @Override
  public BigDecimal valueOf(BigDecimal left, BigDecimal right) {
    if (right.compareTo(BigDecimal.ZERO) == 0) throw new Solver.Exc("division by zero");
    return left.divide(right, 8, RoundingMode.HALF_UP);
  }
}
