package bm.op;

import bm.impl.Solver;

import java.math.BigDecimal;

/* Mateusz Bulak */
public final class Factorial implements PostOp {

  @Override
  public BigDecimal valueOf(BigDecimal left) {
    int cmp = left.compareTo(BigDecimal.ZERO);
    if (cmp < 0) {
      throw new Solver.Exc("factorial argument cannot be zero");
    } else if (left.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) != 0) {
      throw new Solver.Exc("factorial argument is not an integer");
    } else if (cmp == 0 || left.compareTo(BigDecimal.ONE) == 0) {
      return BigDecimal.ONE;
    } else {
      BigDecimal result = new BigDecimal("2");
      BigDecimal iter = new BigDecimal("3");
      while (iter.compareTo(left) <= 0) {
        result = result.multiply(iter);
        iter = iter.add(BigDecimal.ONE);
      }
      return result;
    }
  }
}
