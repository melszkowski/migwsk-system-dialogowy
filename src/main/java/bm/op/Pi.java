package bm.op;

import java.math.BigDecimal;

import static java.lang.Math.PI;

/* Mateusz Bulak */
public final class Pi implements Const {
  private static final BigDecimal val = new BigDecimal(PI);

  @Override
  public BigDecimal valueOf() {
    return val;
  }
}
