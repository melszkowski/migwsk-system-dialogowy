package bm.op;

import java.math.BigDecimal;

/* Mateusz Bulak */
public final class Addition implements BinOp{
  @Override
  public BigDecimal valueOf(BigDecimal left, BigDecimal right) {
    return left.add(right);
  }
}
