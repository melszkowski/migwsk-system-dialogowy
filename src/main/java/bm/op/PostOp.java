package bm.op;

import bm.impl.Expr;

import java.math.BigDecimal;

/* Mateusz Bulak */
@FunctionalInterface
public interface PostOp extends Expr {
  BigDecimal valueOf(BigDecimal left);
}