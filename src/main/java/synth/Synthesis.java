package synth;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceDirectory;
import com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory;

public class Synthesis {


    public void say(String message) {

        VoiceDirectory vd = new KevinVoiceDirectory();
        Voice voice;

        if (vd.getVoices().length > 1) {
            //vd.getVoices[1] is clearer 16kHz voice compared to 8kHz placed in vd.getVoices[0]
            voice = vd.getVoices()[1];
            voice.allocate();
            voice.speak(message);
            voice.deallocate();
        }


    }

}
