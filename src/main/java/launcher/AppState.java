package launcher;

public enum AppState {
    READY, CHOOSE_OPTION, CALCULATE, WEATHER, CHROME;
}
