package launcher;

import bm.Driver;
import bm.DriverFactory;
import synth.Synthesis;
import weather.WeatherForecaster;
import chrome.*;

import java.io.IOException;

public class Controller {

    static AppState state;
    static Synthesis synth;
    private ChromeSearch cSearch;
    static Passable p;
    static Closeable c;

    public Controller(AppState state, Synthesis synth) {
        this.state = state;
        this.synth = synth;
        start();
    }

    private void start(){
        this.setPassable(s -> this.defaultPass(s));
        this.setCloseable(this::defaultClose);
        setAppState(AppState.READY);
    }

    private void restart(){
        start();
    }

    public void accept(String input) throws IOException {
        //any moment a user can say "exit" to go back to inital state
        if(input.equals("exit")){
            c.close();
            if(state != AppState.READY) {
                synth.say("exiting.");
            }
            restart();
            return;
        }

        switch (state) {

            case READY:
                if (input.equals(Global.NAME)) {
                    state = AppState.CHOOSE_OPTION;
                    synth.say("yes?");
                }
                break;
            case CHOOSE_OPTION:
                handleOption(input);
                break;
            case CALCULATE:
            case WEATHER:
                p.pass(input);
                break;
            case CHROME:
                this.cSearch.accept(input);
        }
    }

    private void handleOption(String input) {
        try {
            state = AppState.valueOf(input.toUpperCase());
            synth.say("starting " + state.toString().toLowerCase());
            startFeature(state);
        } catch (IllegalArgumentException e) {
            state = AppState.CHOOSE_OPTION;
            synth.say("wrong option, please repeat.");
        }

    }

    private void startFeature(AppState s) {
        switch (s) {
            case CALCULATE:
                runCalculator();
                break;
            case WEATHER:
                runWeather();
                break;
            case CHROME:
                runChrome();
                break;
        }
    }

    public void runCalculator() {
        setAppState(AppState.CALCULATE);

        DriverFactory df = DriverFactory.constructDefault();
        Synthesis synth = new Synthesis();

        df.setVoiceable(s -> {
            synth.say(s);
            System.out.println(s);
        });
        Driver d = df.construct();

        this.setPassable(s -> d.accept(s));
        this.setCloseable(d::close);

        d.show();
    }

    public void runWeather() {
        setAppState(AppState.WEATHER);

        WeatherForecaster wf = new WeatherForecaster();
        String forecast = wf.getWeatherForecast();
        synth.say(forecast);
        restart();
    }
    public void runChrome() {
        setAppState(AppState.CHROME);
        cSearch = new ChromeSearch();
        this.setCloseable(cSearch::close);
    }
    private static void setAppState(AppState s) {
        state = s;
    }

    public void setPassable(Passable p) {
        this.p = p;
    }
    public void setCloseable(Closeable c) {
        this.c = c;
    }

    public void defaultClose(){
        System.out.println("Nothing to close.");
    }

    public void defaultPass(String s) {
        System.out.println("No option has been chosen yet.");
    }
}