package launcher;

import bm.*;
import weather.*;
import rec.Recognition;
import synth.Synthesis;

import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.LogManager;

public class Main {


    static Synthesis synth;
    public static Recognition recognition;
    static Controller controller;

    final static String NAME = "david";
    public static void main(String[] args) throws Exception {

        //set all java.util.logging.Logger objects to output only warnings
        Arrays.stream(LogManager.getLogManager().getLogger("").getHandlers()).forEach(h -> h.setLevel(Level.WARNING));
        System.err.close();

        synth = new Synthesis();
        controller = new Controller(AppState.READY, synth);
        recognition = new Recognition(s -> {
            controller.accept(s);
        });
        Thread mainRecThread = new Thread(recognition);
        mainRecThread.start();

        sayHello();

    }

    private static void sayHello(){
        synth.say("Hello, my name is " + NAME+".");
        //synth.say("Hello!");
    }



}