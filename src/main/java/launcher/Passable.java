package launcher;

@FunctionalInterface
public interface Passable {
    void pass(String message);
}
