package launcher;

@FunctionalInterface
public interface Closeable {
    void close();
}

