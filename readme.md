Group project for subject "Mechanisms of voice interaction in computer systems".
It's a dialogue system, that can calculate formulas, tell weather, open few apps (e.g. Internet browser) and is easily extendable.

Voice recognition - CMU Sphinx 4
Voice synthesis - FreeTTS

_____________________________________

Do poprawnego działania modułu pogodowego aplikacja musi być skompilowana z opcją 
--add-modules java.xml.bind